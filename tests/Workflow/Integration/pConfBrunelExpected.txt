from ProdConf import ProdConf

ProdConf(
  NOfEvents=-1,
  DDDBTag='Sim08-20130503-1',
  CondDBTag='Sim08-20130503-1-vc-mu100',
  AppVersion='v43r2p7',
  XMLSummaryFile='summaryBrunel_00012345_00006789_4.xml',
  Application='Brunel',
  OutputFilePrefix='00012345_00006789_4',
  XMLFileCatalog='pool_xml_catalog.xml',
  InputFiles=['LFN:00012345_00006789_3.digi'],
  OutputFileTypes=['dst'],
)