Developing DIRAC and LHCbDIRAC
===============================

.. toctree::
   :maxdepth: 2

Developing the code is not just about editing. You also want to "run" something, usually for testing purposes.
The DIRAC way of developing can be found `here <http://dirac.readthedocs.io/en/latest/DeveloperGuide/index.html>`_
and it applies also to LHCbDIRAC. Please follow carefully especially what's
`here <http://dirac.readthedocs.io/en/latest/DeveloperGuide/DevelopmentEnvironment/DeveloperInstallation/index.html>`_

In general, if you are developing LHCbDIRAC, you should consider that:

  - everything that applies to DIRAC development, also applies to LHCbDIRAC development, so, follow carefully the links above
  - every LHCbDIRAC release has a strong dependency with a DIRAC release. See https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/blob/master/CONTRIBUTING.md for more info.
